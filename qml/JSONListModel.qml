/* JSONListModel - a QML ListModel with JSON and JSONPath support
 *
 * Copyright (c) 2012 Romain Pokrzywka (KDAB) (romain@kdab.com)
 * Licensed under the MIT licence (http://opensource.org/licenses/mit-license.php)
 * modified by Szymon Waliczek <majsterrr@gmail.com>
 */

import QtQuick 2.9
import Morph.Web 0.1
import QtWebEngine 1.7
import Ubuntu.Connectivity 1.0
import Ubuntu.Components.Popups 1.3

import "jsonpath.js" as JSONPath

FocusScope {
    id: root
    visible: false

    property string    source: ""
    property string    query: ""
    property int       status: 3 // 0 = Loading, 1 = ready , 2 = Error, 3 = NoStatus, 4 = CheckingInternet
    property string    jsonString: ""
    property bool      isArray: false
    property ListModel model: ListModel {}

    property ListModel recentTitles: ListModel {}
    property string    family: ""
    property string    language: ""
    property string    topics: ""
    property string    genres: ""

    property bool      openDialog: false
onSourceChanged: console.log(".--.............",source)
    onOpenDialogChanged: {
        if(openDialog) {
            PopupUtils.open(noInternetPopOver)
        } else {
            PopupUtils.close(noInternetPopOver)
        }
    }

    function resetArrayData() {
        recentTitles.clear()
        family = ""
        language = ""
        topics = ""
        genres = ""
    }

//    onStatusChanged: {
//        print("----------------------------------")
//        print("status is: ", status)
//        print("----------------------------------")
//    }

    Connections {
        target: Connectivity
        // full status can be retrieved from the base C++ class status property
        // onStatusChanged: console.log("Status: " + statusMap[Connectivity.status])
        onOnlineChanged: {
            if(Connectivity.online) {
                reload()
                mediaHub.playOrPause()
            } else {
                isLoaded = false
                root.status = 2
                mediaHub.stop()
            }

            console.log("Online: " + Connectivity.online)
        }
    }

    function reload() {
        root.status = 0
        var a = source
        root.source = ""
        root.source = a
    }

    function loadDelay() {
        loadListDelay.start()
    }

    function parseJSON(json, query) {
        var jsonData

        try {
            jsonData = JSON.parse(json, query);
            if(query !== "") jsonData = JSONPath.jsonPath(jsonData, query)

            console.log("[LOG]","JSON parsing: OK")
            return jsonData
        } catch(e){
            root.status = 2
            console.error("[ERROR]", "parsing JSON")
        }
    }

    function loadList() {
        model.clear();
        var objectArray = parseJSON(jsonString, query);

        if(!isArray) {
            for ( var key in objectArray ) {
                var jo = objectArray[key];
                model.append( jo );
                root.status = 1
            }
        } else {
            resetArrayData()

            try {
                for(var key2 in objectArray[0]) {
                    var jo2 = objectArray[0]

                    for (var key3 in jo2) {
                        try {
                            switch(key3) {
                              case "family":
                                family = jo2.family[0]
                                break;
                              case "recentTitles":
                                for (var r1 in key3) {
                                    if(typeof jo2.recentTitles[r1] != "undefined") {
                                        recentTitles.append({
                                            "trackName": jo2.recentTitles[r1]
                                        })
                                    }
                                }
                                break;
                              case "language":
                                language = jo2.language[0];
                                break;
                              case "topics":
                                topics = jo2.topics[0];
                                break;
                              case "genres":
                                genres = jo2.genres[0]
                                break;
                            }
                        } catch(err) {
                            console.log("DEBUG: loadList Error:", err)
                        }
                    }

                    model.append(jo2)
                    root.status = 1
                    break
                }
            } catch(err) {
                root.status = 2
                console.log("[ERROR] Loading Error!")
            }
        }
    }

    Timer {
        id: loadListDelay
        interval: 200
        repeat: false
        onTriggered: loadList()
    }

    onQueryChanged: {
        if(root.status != 3) {
            isLoaded = false
            loadListDelay.start()
        }
    }

    WebContext {
        id: webcontext
        userAgent: "XBMC Addon Radio"
    }

    WebView {
        id: webview
        visible: false
        context: webcontext
        url: source

        //Used onLoadingChanged instead of onLoadProgressChanged
        onLoadingChanged: {
            if (!loading) {
                webview.runJavaScript("document.documentElement.innerHTML", function callback(code) {
                    jsonString = code.slice(code.search(';">')+3)
                    jsonString = jsonString.slice(0, jsonString.search('</pre'))

                    if(root.status != 2) {
                        loadList()
                    }
                })
            }
        }
    }
}
