import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
    id: favoritesPage

    header: PageHeader {
        title: i18n.tr("Favorites")
        flickable: null
    }

    Flickable {
        anchors {
            fill: parent
            topMargin: parent.header.height + units.gu(3)
            bottomMargin: mediaPlayerBottomEdge.playerHeight + units.gu(2)
        }

        contentHeight: lol.height
        contentWidth: parent.width

        Column {
            id: lol
            width: parent.width - units.gu(4)
            anchors.horizontalCenter: parent.horizontalCenter
            visible: favoritesListModel.count < 1
            spacing: units.gu(2)

            Label {
                width: parent.width
                height: units.gu(7)
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                fontSize: "x-large"
                text: i18n.tr("No favorites saved yet")
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                fontSize: "large"
                text: i18n.tr("Find radio in the main list then swipe it left and click on ★")
            }

            Image {
                width: parent.width
                fillMode: Image.PreserveAspectFit
                source: Qt.resolvedUrl("../assets/hint1.png")
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                fontSize: "x-large"
                text: i18n.tr("or")
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                fontSize: "large"
                text: i18n.tr("Click on radio station and then press ★ symbol in the top right corner.")
            }

            Image {
                width: parent.width
                fillMode: Image.PreserveAspectFit
                source: Qt.resolvedUrl("../assets/hint2.png")
            }
        }
    }

    ListView {
        anchors {
            fill: parent
            topMargin: parent.header.height
        }

        id: favoritesListView
        model: favoritesListModel
        visible: favoritesListModel.count > 0
        delegate: favoriteComponent

        add: Transition {
            NumberAnimation { properties: "y"; duration: 500 }
        }

        displaced: Transition {
            NumberAnimation { properties: "y"; duration: 500 }
        }
    }

    Component {
        id: favoriteComponent
        FavoriteListItemDelegate {}
    }
}
