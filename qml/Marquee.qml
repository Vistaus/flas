import QtQuick 2.9
import Ubuntu.Components 1.3

Item {
    id: root
    height: marqueeText.height + padding
    clip: true

    // text to be displayed by the Marquee
    property string text

    property alias labe: marqueeText

    // top/bottom text padding
    property int padding : 10

    // the font size used by the Marquee ( "xx-small", "x-small", "small", "medium", "large", "x-large")
    property string marqueeFontSize : "large"

    // the scrolling animation speed
    property int scrollingSpeed : 8

    // pouse in scrolling time in ms
    property int pauseTime: 2000

    // the text color
    property color textColor: theme.palette.normal.backgroundText

    // color of the listview BG
    property color bgColor: "transparent"

    // stopAnimationOnClick
    property bool stopOnClick: false

    onTextChanged: marqueeText.x = 0

    Label {
        id: marqueeText
        anchors.verticalCenter: parent.verticalCenter
        fontSize: marqueeFontSize
        color: textColor
        text: root.text
        x: 0

        SequentialAnimation on x {
            id: xAnim
            running: marqueeText.width > root.width
            // The animation is set to loop indefinitely
            loops: Animation.Infinite

            PauseAnimation {
                duration: pauseTime
            } // pausa before scrolling

            NumberAnimation {
                from: 0; to: -(marqueeText.width - root.width)
                duration: marqueeText.width * scrollingSpeed
            }

            PauseAnimation {
                duration: pauseTime
            } // brake in the end of the scrooling

            NumberAnimation {
                from: -(marqueeText.width - root.width)
                to: 0
                duration: 500
                easing.type: Easing.InOutQuad
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        enabled: stopOnClick

        onClicked: {
            xAnim.paused === false
                ? xAnim.pause()
                : xAnim.resume()
        }
    }

}
